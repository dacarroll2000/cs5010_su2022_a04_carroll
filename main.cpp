#include <iostream>
#include <string>

#include "Mathy.h"

template<typename swapers>
void Swap (swapers &swap1, swapers &swap2);

template<typename multipliers>
void Multiples (multipliers &sum, multipliers x, int n);

int main() {
    //variables that I will swap
    int intSwap1 = 1;
    int intSwap2 = 0;
    std::string stringSwap1 = "David";
    std::string stringSwap2 = "Carroll";
    double doubleSwap1 = 0.1;
    double doubleSwap2 = 0.2;

    //variables used with Multiples
    int intSum = 0;
    int intx = 10;
    int intn = 5;
    double doubleSum = 0.0;
    double doublex = 0.1;
    int doublen = 5;

    //testing Swap template function with three variable types
    std::cout << "Testing Swap template function with int:" << std::endl;
    std::cout << "Before swap: " << intSwap1 << " " << intSwap2 << std::endl;
    Swap(intSwap1, intSwap2);
    std::cout << "After swap: " << intSwap1 << " " << intSwap2 << std::endl;

    std::cout << std::endl << "Testing Swap template function with string:" << std::endl;
    std::cout << "Before swap: " << stringSwap1 << " " << stringSwap2 << std::endl;
    Swap(stringSwap1, stringSwap2);
    std::cout << "After swap: " << stringSwap1 << " " << stringSwap2 << std::endl;

    std::cout << std::endl << "Testing Swap template function with double:" << std::endl;
    std::cout << "Before swap: " << doubleSwap1 << " " << doubleSwap2 << std::endl;
    Swap(doubleSwap1, doubleSwap2);
    std::cout << "After swap: " << doubleSwap1 << " " << doubleSwap2 << std::endl;

    //testing Multiples template function with two variable types
    std::cout << std::endl << "Testing Multiples template function with int:" << std::endl;
    std::cout << "Sum before: " << intSum << ", x before:  " << intx << ", n before: " << intn << std::endl;
    Multiples(intSum, intx, intn);
    std::cout << "Sum after: " << intSum << ", x after:  " << intx << ", n after:  " << intn << std::endl;

    std::cout << std::endl << "Testing Multiples template function with double:" << std::endl;
    std::cout << "Sum before: " << doubleSum << ", x before: " << doublex << ", n before: " << doublen << std::endl;
    Multiples(doubleSum, doublex, doublen);
    std::cout << "Sum after: " << doubleSum << ", x after: " << doublex << ", n after: " << doublen << std::endl;

    //all variables will be reset to their original values for easy comparison between class/function
    std::cout << std::endl << "\nAll variables will be reset to their original values for easy comparison between class/function\n" << std::endl;
    intSwap1 = 1;
    intSwap2 = 0;
    stringSwap1 = "David";
    stringSwap2 = "Carroll";
    doubleSwap1 = 0.1;
    doubleSwap2 = 0.2;
    intSum = 0;
    intx = 10;
    intn = 5;
    doubleSum = 0.0;
    doublex = 0.1;
    doublen = 5;


    //testing Swap template class function with three variable types
    //static member function was used, so I could just call the functions directly instead of creating an object
    std::cout << std::endl << "Testing Swap template class function with int:" << std::endl;
    std::cout << "Before swap: " << intSwap1 << " " << intSwap2 << std::endl;
    Mathy<int>::Swap(intSwap1, intSwap2);
    std::cout << "After swap: " << intSwap1 << " " << intSwap2 << std::endl;

    std::cout << std::endl << "Testing Swap template class function with double:" << std::endl;
    std::cout << "Before swap: " << doubleSwap1 << " " << doubleSwap2 << std::endl;
    Mathy<double>::Swap(doubleSwap1, doubleSwap2);
    std::cout << "After swap: " << doubleSwap1 << " " << doubleSwap2 << std::endl;

    std::cout << std::endl << "Testing Swap template class function with string:" << std::endl;
    std::cout << "Before swap: " << stringSwap1 << " " << stringSwap2 << std::endl;
    Mathy<std::string>::Swap(stringSwap1, stringSwap2);
    std::cout << "After swap: " << stringSwap1 << " " << stringSwap2 << std::endl;

    //testing Multiples template class function with two variable types
    std::cout << std::endl << "Testing Multiples template class function with int:" << std::endl;
    std::cout << "Sum before: " << intSum << ", x before:  " << intx << ", n before: " << intn << std::endl;
    Mathy<int>::Multiples(intSum, intx, intn);
    std::cout << "Sum after: " << intSum << ", x after:  " << intx << ", n after:  " << intn << std::endl;

    std::cout << std::endl << "Testing Multiples template class function with double:" << std::endl;
    std::cout << "Sum before: " << doubleSum << ", x before: " << doublex << ", n before: " << doublen << std::endl;
    Mathy<double>::Multiples(doubleSum, doublex, doublen);
    std::cout << "Sum after: " << doubleSum << ", x after: " << doublex << ", n after: " << doublen << std::endl;
    return 0;
}

template<typename swapers>
void Swap (swapers &swap1, swapers &swap2) {
    swapers tmpSwapper;
    tmpSwapper = swap2;
    swap2 = swap1;
    swap1 = tmpSwapper;
}

template<typename multipliers>
void Multiples (multipliers &sum, multipliers x, int n) {
    //sum = 1 + 1x + 2x + ... + nx
    sum = 1;
    for (int i = 1; i <= n; ++i) {
        sum += i*x;
    }
}