output from main.cpp:

Testing Swap template function with int:
Before swap: 1 0
After swap: 0 1

Testing Swap template function with string:
Before swap: David Carroll
After swap: Carroll David

Testing Swap template function with double:
Before swap: 0.1 0.2
After swap: 0.2 0.1

Testing Multiples template function with int:
Sum before: 0, x before:  10, n before: 5
Sum after: 151, x after:  10, n after:  5

Testing Multiples template function with double:
Sum before: 0, x before: 0.1, n before: 5
Sum after: 2.5, x after: 0.1, n after: 5


All variables will be reset to their original values for easy comparison between class/function


Testing Swap template class function with int:
Before swap: 1 0
After swap: 0 1

Testing Swap template class function with double:
Before swap: 0.1 0.2
After swap: 0.2 0.1

Testing Swap template class function with string:
Before swap: David Carroll
After swap: Carroll David

Testing Multiples template class function with int:
Sum before: 0, x before:  10, n before: 5
Sum after: 151, x after:  10, n after:  5

Testing Multiples template class function with double:
Sum before: 0, x before: 0.1, n before: 5
Sum after: 2.5, x after: 0.1, n after: 5