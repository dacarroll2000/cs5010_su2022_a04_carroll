#ifndef MATHY_H
#define MATHY_H

template <typename swapersMultipliers>
class Mathy {
public:
    //static functions were used so creating objects in main.cpp is not required
    static void Swap(swapersMultipliers &swap1, swapersMultipliers &swap2) {
        swapersMultipliers tmpSwapper;
        tmpSwapper = swap2;
        swap2 = swap1;
        swap1 = tmpSwapper;
    }
    static void Multiples(swapersMultipliers &sum, swapersMultipliers x, int n){
        sum = 1;
        for (int i = 1; i <= n; ++i) {
            sum += i*x;
        }
    }
};

#endif